# Shakespearization

## Code organisation

The application is divided into 6 projects:

- Shakespearization.Api: Entry point of the application
- Shakespearization.Domain: Where the domain model and Requests are defined
- Shakespearization.Pokemon: Where the RequestHandler to retrieve a Pokemon is implemented
- Shakespearization.Funstralations.Api: Where the call to funstralations api is made
- Shakespearization.Pokemon.Api: Where the calls to the pokeapi are made
- Shakespearization.LocalCache: Where pokemons are stored locally

## Implementation

The whole application lies upon [MediatR](https://github.com/jbogard/MediatR) using Requests, RequestHandlers and PipelineBehaviors to perform the operation to retrieve a Pokemon with its translated description.
Rest Api requests are performed with [Flurl](https://flurl.dev/) as is easy to [use](https://gitlab.com/dariogriffo/shakespearization/-/blob/main/src/Shakespearization.FunTranslations.Api/Pipeline/GetShakespeareanDescriptionBehavior.cs#L34) and easy to [test](https://gitlab.com/dariogriffo/shakespearization/-/blob/main/tests/Shakespearization.FunTranslations.Api.UnitTests/GetShakespeareanDescriptionHandlerTests.cs#L24)

## Patterns

Like any case where requests to external resources are made, a CircuitBreaker is implemented with [Polly](https://github.com/App-vNext/Polly)
Also the throttling error handling of the funtranslations api is handled with Polly via a WaitAndRetry
Policies can be seen [here](https://gitlab.com/dariogriffo/shakespearization/-/tree/main/src/Shakespearization.FunTranslations.Api/Policies)


## LocalCache
In order to prevent repeated calls for the same Pokemon a local storage is implemented with a IPipelineBehavior [here](https://gitlab.com/dariogriffo/shakespearization/-/blob/main/src/Shakespearization.LocalCache/Pipeline/GetCachedPokemonBehavior.cs#L23)

## Prerequisites
Windows or linux please install Docker
Linux please install curl
a script to run on Mac is missing

## Running the application

- Make sure that docker is installed and running.
-  Clone this repo
```
   git clone https://gitlab.com/dariogriffo/shakespearization
   cd shakespearization
   git checkout master
```
Windows
- Open a windows powershell and navigate to the repository's location
- run .\build.ps1

Linux
- Open a terminal and navigate to the repository's location
- run . build.sh

# Application running

At this moment in the terminal messages can be seen of the application running and listening on ports 5000 and 5001.
First a [health check](https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks?view=aspnetcore-5.0) can be performed
Please open a browser or perform a rest request from the preferred rest client to
 ``http://localhost:5000/health``
if everything is ok a message
``Healthy``
should be seen.

Also open api documentation can be accessed via browser at
``http://localhost:5000/swagger``

Let's retrieve the first pokemon:
Perform a rest request from the preferred rest client to
``http://localhost:5000/pokemon/ditto``

The answer should be a json document with structure
```
{
    "name" : "ditto",
    "description" : "shakespearean description"
}
 ```

Now perform a request to
``http://localhost:5000/dario``

The answer should be a NotFound response with the name dario


## Testing

2 projects containing Unit Tests and Integration Tests can be found in the tests folder.
These tests are run before the application starts when running the script mentioned above.
To test the LocalCache [Mongo2Go](https://github.com/Mongo2Go/Mongo2Go) was used [here](https://gitlab.com/dariogriffo/shakespearization/-/blob/main/tests/Shakespearization.LocalCache.IntegrationTests/GetShakespeareanDescriptionHandlerCacheTests.cs#L19)

For testing Nunit/Moq/FluentAssertions are the frameworks of choice.


## Improvements

- Documentation: Code is not properly commented/documented, even though open api is enabled and the model/action was tagged to provide information.
- Better error handling in DataAccess (concurrency to be tested)
- Indexes in the LocalCache
-
## Request flow

```mermaid
sequenceDiagram
Controller ->> Cache Behavior: GetPokemon
Cache Behavior -->> Cache Behavior: Cached?
Cache Behavior ->> Controller: Yes, Return Pokemon
Cache Behavior -->> TranslationFunBehavior: No, GetPokemon
TranslationFunBehavior -->> PokeApiHandler: GetPokemon
Note right of PokeApiHandler: English description is created as join of all pokemon's abilities with a new line
PokeApiHandler ->>TranslationFunBehavior : return Pokemon
TranslationFunBehavior -->> TranslationFunApi: GetTranslation
TranslationFunApi ->>TranslationFunBehavior : return Translation
TranslationFunBehavior -->> Cache Behavior: return Translation
Cache Behavior -->> Cache Behavior: Save
Cache Behavior -->> Controller: return  Pokemon``
