﻿namespace Shakespearization.Domain.Requests
{
    using MediatR;
    using Shakespearization.Domain.Model;

    public class GetPokemon : IRequest<Pokemon>
    {
        public GetPokemon(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
