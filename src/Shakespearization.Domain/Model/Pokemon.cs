﻿namespace Shakespearization.Domain.Model
{
    public class Pokemon
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string TranslatedDescription { get; set; }
    }
}
