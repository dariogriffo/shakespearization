﻿namespace Shakespearization.FunTranslations.Api
{
    using Shakespearization.FunTranslations.Api.Request;

    public interface IFunTranslationsRequestCreator
    {
        Body Create(string text);
    }
}
