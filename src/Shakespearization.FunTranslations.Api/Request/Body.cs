﻿namespace Shakespearization.FunTranslations.Api.Request
{
    using Newtonsoft.Json;

    public class Body
    {
        public Body(string text)
        {
            Text = text;
        }

        [JsonProperty("text")]
        public string Text { get; }
    }
}
