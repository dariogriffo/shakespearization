﻿namespace Shakespearization.FunTranslations.Api.Policies
{
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using Polly;

    public class PolicyHandler : DelegatingHandler
    {
        private readonly IAsyncPolicy<HttpResponseMessage> _policy;

        public PolicyHandler(IPolicyProvider policies)
        {
            _policy = policies.GetShakespeareanDescriptionPolicy();
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return _policy.ExecuteAsync(ct => base.SendAsync(request, ct), cancellationToken);
        }
    }
}
