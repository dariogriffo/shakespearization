﻿namespace Shakespearization.FunTranslations.Api.Policies
{
    using System.Net.Http;
    using Flurl.Http.Configuration;

    public class PollyHttpClientFactory : DefaultHttpClientFactory
    {
        private readonly IPolicyProvider _policyProvider;

        public PollyHttpClientFactory(IPolicyProvider policyProvider)
        {
            _policyProvider = policyProvider;
        }

        public override HttpMessageHandler CreateMessageHandler()
        {
            return new PolicyHandler(_policyProvider)
            {
                InnerHandler = base.CreateMessageHandler(),
            };
        }
    }
}
