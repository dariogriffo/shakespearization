﻿namespace Shakespearization.FunTranslations.Api.Policies
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using Polly;

    public class PolicyProvider : IPolicyProvider
    {
        private static readonly List<HttpStatusCode> HttpStatusCodesWorthRetrying = new List<HttpStatusCode>
        {
            HttpStatusCode.InternalServerError, // 500
            HttpStatusCode.BadGateway, // 502
            HttpStatusCode.GatewayTimeout, // 504
        };

        public IAsyncPolicy<HttpResponseMessage> GetShakespeareanDescriptionPolicy()
        {
            var waitAndRetryPolicy = Policy
                .HandleResult<HttpResponseMessage>(e => e.StatusCode == HttpStatusCode.ServiceUnavailable ||
                                                        e.StatusCode == (System.Net.HttpStatusCode)429)
                .WaitAndRetryAsync(
                    6, // Retry every 10 minutes
                    attempt => TimeSpan.FromMinutes(10),
                    (exception, calculatedWaitDuration) =>
                    {
                    });

            var circuitBreakerPolicyForRecoverable = Policy
                .Handle<Exception>()
                .OrResult<HttpResponseMessage>(r => HttpStatusCodesWorthRetrying.Contains(r.StatusCode))
                .CircuitBreakerAsync(
                    handledEventsAllowedBeforeBreaking: 3,
                    durationOfBreak: TimeSpan.FromSeconds(3),
                    onBreak: (outcome, breakDelay) =>
                    {
                    },
                    onReset: () => { },
                    onHalfOpen: () =>
                    {
                    });

            return Policy.WrapAsync(waitAndRetryPolicy, circuitBreakerPolicyForRecoverable);
        }
    }
}
