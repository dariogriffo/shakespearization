﻿namespace Shakespearization.FunTranslations.Api.Policies
{
    using System.Net.Http;
    using Polly;

    public interface IPolicyProvider
    {
        IAsyncPolicy<HttpResponseMessage> GetShakespeareanDescriptionPolicy();
    }
}
