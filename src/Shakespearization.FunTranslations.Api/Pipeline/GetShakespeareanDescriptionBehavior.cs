﻿namespace Shakespearization.FunTranslations.Api.Pipeline
{
    using System.Threading;
    using System.Threading.Tasks;
    using Flurl.Http;
    using MediatR;
    using Microsoft.Extensions.Logging;
    using Shakespearization.Domain.Model;
    using Shakespearization.Domain.Requests;
    using Shakespearization.FunTranslations.Api.Configuration;
    using Shakespearization.FunTranslations.Api.Models.Response;

    public class GetShakespeareanDescriptionBehavior
        : IPipelineBehavior<GetPokemon, Domain.Model.Pokemon>
    {
        private readonly FunTranslationsApiConfiguration _configuration;
        private readonly IFunTranslationsRequestCreator _requestCreator;
        private readonly ILogger<GetShakespeareanDescriptionBehavior> _logger;

        public GetShakespeareanDescriptionBehavior(
            IFunTranslationsRequestCreator requestCreator,
            ILogger<GetShakespeareanDescriptionBehavior> logger,
            FunTranslationsApiConfiguration configuration)
        {
            _configuration = configuration;
            _requestCreator = requestCreator;
            _logger = logger;
        }

        public async Task<Pokemon> Handle(GetPokemon request, CancellationToken cancellationToken, RequestHandlerDelegate<Pokemon> next)
        {
            var pokemon = await next();
            var body = _requestCreator.Create(pokemon.Description);
            var root = await
                _configuration.Url
                    .PostJsonAsync(body, cancellationToken)
                    .ReceiveJson<Root>();

            if (root.Success.Total > 0)
            {
                _logger.LogDebug("Found translation for Pokemon {0}", pokemon.Name);
                pokemon.TranslatedDescription = root.Contents.Translated;
            }
            else
            {
                _logger.LogWarning("Translation for Pokemon {0} not found", pokemon.Name);
                pokemon.TranslatedDescription = pokemon.Description;
            }

            return pokemon;
        }
    }
}
