﻿namespace Shakespearization.FunTranslations.Api
{
    using System;
    using Shakespearization.FunTranslations.Api.Request;

    public class FunTranslationsRequestCreator : IFunTranslationsRequestCreator
    {
        public Body Create(string text)
        {
            _ = text ?? throw new ArgumentNullException(nameof(text));
            return new Body(text);
        }
    }
}
