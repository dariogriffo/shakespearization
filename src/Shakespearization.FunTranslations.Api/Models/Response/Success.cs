﻿namespace Shakespearization.FunTranslations.Api.Models.Response
{
    public class Success
    {
        public int Total { get; set; }
    }
}
