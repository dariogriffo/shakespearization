﻿namespace Shakespearization.FunTranslations.Api.Models.Response
{
    public class Root
    {
        public Success Success { get; set; }

        public Contents Contents { get; set; }
    }
}
