﻿namespace Shakespearization.FunTranslations.Api
{
    using Flurl.Http;
    using Flurl.Http.Configuration;
    using MediatR;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using ServiceCollection.Extensions.Modules;
    using Shakespearization.Domain.Requests;
    using Shakespearization.FunTranslations.Api.Configuration;
    using Shakespearization.FunTranslations.Api.Pipeline;
    using Shakespearization.FunTranslations.Api.Policies;

    public class FunTranslationsApiModule : Module
    {
        protected override void Load(IServiceCollection services)
        {
            base.Load(services);
            services.AddSingleton<IPolicyProvider, PolicyProvider>();
            services.AddSingleton<IFunTranslationsRequestCreator, FunTranslationsRequestCreator>();
            services.AddSingleton(c => c.GetService<IConfiguration>().GetSection("FunTranslationsApi").Get<FunTranslationsApiConfiguration>());
            services.AddSingleton<IHttpClientFactory, PollyHttpClientFactory>();
            FlurlHttp.Configure(settings =>
            {
                settings.HttpClientFactory = services.BuildServiceProvider().GetService<IHttpClientFactory>();
            });

            services.AddScoped<IPipelineBehavior<GetPokemon, Domain.Model.Pokemon>, GetShakespeareanDescriptionBehavior>();
        }
    }
}
