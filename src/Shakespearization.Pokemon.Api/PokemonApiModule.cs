﻿namespace Shakespearization.Pokemon.Api
{
    using MediatR;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using ServiceCollection.Extensions.Modules;
    using Shakespearization.Domain.Requests;

    public class PokemonApiModule : Module
    {
        protected override void Load(IServiceCollection services)
        {
            base.Load(services);
            services.AddSingleton(c => c.GetService<IConfiguration>().GetSection("PokemonApi").Get<PokemonApiConfiguration>());
            services.AddScoped<IPipelineBehavior<GetPokemon, Domain.Model.Pokemon>, PokemonValidator>();
        }
    }
}
