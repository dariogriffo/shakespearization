﻿namespace Shakespearization.Pokemon.Api
{
    using System;

    public class PokemonNotFoundException : Exception
    {
        public string Pokemon { get; }

        public PokemonNotFoundException(string pokemon)
        {
            Pokemon = pokemon;
        }
    }
}
