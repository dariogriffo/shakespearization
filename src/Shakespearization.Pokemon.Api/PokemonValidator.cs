﻿namespace Shakespearization.Pokemon.Api
{
    using System.Threading;
    using System.Threading.Tasks;
    using Flurl.Http;
    using MediatR;
    using Shakespearization.Domain.Requests;

    public class PokemonValidator : IPipelineBehavior<GetPokemon, Domain.Model.Pokemon>
    {
        private readonly PokemonApiConfiguration _configuration;

        public PokemonValidator(PokemonApiConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<Domain.Model.Pokemon> Handle(GetPokemon request, CancellationToken cancellationToken, RequestHandlerDelegate<Domain.Model.Pokemon> next)
        {
            var format = string.Format(_configuration.Url, request.Name);
            var result = await format.AllowHttpStatus("404").GetAsync(cancellationToken);
            if (result.StatusCode < 300)
            {
                return await next();
            }

            throw new PokemonNotFoundException(request.Name);
        }
    }
}
