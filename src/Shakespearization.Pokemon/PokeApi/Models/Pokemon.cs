﻿namespace Shakespearization.Pokemon.PokeApi.Models
{
    using System.Collections.Generic;

    public class Pokemon
    {
        public List<Ability> abilities { get; set; }

        public int id { get; set; }

        public string name { get; set; }

        public NameAndUrl species { get; set; }
    }
}
