﻿namespace Shakespearization.Pokemon.PokeApi.Models
{
    using System.Collections.Generic;

    public class Ability
    {
        public NameAndUrl ability { get; set; }

        public bool is_hidden { get; set; }

        public int slot { get; set; }

        public List<EffectEntry> effect_entries { get; set; }
    }
}
