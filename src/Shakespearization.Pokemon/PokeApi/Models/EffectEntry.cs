﻿namespace Shakespearization.Pokemon.PokeApi.Models
{
    public class EffectEntry
    {
        public string effect { get; set; }

        public NameAndUrl language { get; set; }

        public string short_effect { get; set; }
    }
}
