﻿namespace Shakespearization.Pokemon.PokeApi.Models
{
    public class FlavorTextEntry
    {
        public string flavor_text { get; set; }

        public NameAndUrl language { get; set; }
    }
}
