﻿namespace Shakespearization.Pokemon.PokeApi.Models
{
    public class NameAndUrl
    {
        public string name { get; set; }

        public string url { get; set; }
    }
}
