﻿namespace Shakespearization.Pokemon
{
    public interface IDescriptionGenerator
    {
        string GenerateDescription(PokeApi.Models.Pokemon apiModel);
    }
}
