﻿namespace Shakespearization.Pokemon
{
    using System;
    using System.Linq;

    public class DescriptionGenerator : IDescriptionGenerator
    {
        public string GenerateDescription(PokeApi.Models.Pokemon apiModel)
        {
            var abilities =
                apiModel
                    .abilities
                    .SelectMany(x => x.effect_entries)
                    .Where(x => x.language?.name == "en")
                    .Select(x => x.effect)
                    .Select(x => x.Replace("\\n", Environment.NewLine));

            return string.Join(Environment.NewLine, abilities);
        }
    }
}
