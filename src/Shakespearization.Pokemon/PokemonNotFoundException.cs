﻿namespace Shakespearization.Pokemon
{
    using System;

    public class PokemonNotFoundException : Exception
    {
        public PokemonNotFoundException(string pokemon)
        {
            Pokemon = pokemon;
        }

        public string Pokemon { get; }
    }
}
