﻿namespace Shakespearization.Pokemon
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using ServiceCollection.Extensions.Modules;

    public class PokemonApiModule : Module
    {
        protected override void Load(IServiceCollection services)
        {
            base.Load(services);
            services.AddSingleton<IDescriptionGenerator, DescriptionGenerator>();
            services.AddSingleton(c => c.GetService<IConfiguration>().GetSection("PokemonApi").Get<PokemonApiConfiguration>());
        }
    }
}
