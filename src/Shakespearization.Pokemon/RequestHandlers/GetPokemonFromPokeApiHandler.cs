﻿namespace Shakespearization.Pokemon.RequestHandlers
{
    using System.Threading;
    using System.Threading.Tasks;
    using Flurl.Http;
    using MediatR;
    using Shakespearization.Domain.Requests;
    using Shakespearization.Pokemon.PokeApi.Models;

    public class GetPokemonFromPokeApiHandler
        : IRequestHandler<GetPokemon, Domain.Model.Pokemon>
    {
        private readonly IDescriptionGenerator _descriptionGenerator;
        private readonly PokemonApiConfiguration _configuration;

        public GetPokemonFromPokeApiHandler(IDescriptionGenerator descriptionGenerator, PokemonApiConfiguration configuration)
        {
            _descriptionGenerator = descriptionGenerator;
            _configuration = configuration;
        }

        public async Task<Domain.Model.Pokemon> Handle(GetPokemon request, CancellationToken cancellationToken)
        {
            var url = $"{_configuration.Url}/pokemon/{request.Name}";
            var result = await url.AllowHttpStatus("404").GetAsync(cancellationToken);
            if (result.StatusCode < 300)
            {
                var apiModel = await result.GetJsonAsync<PokeApi.Models.Pokemon>();
                foreach (var abilityTag in apiModel.abilities)
                {
                    url = abilityTag.ability.url;
                    var ability = await url.GetJsonAsync<Ability>(cancellationToken);
                    abilityTag.effect_entries = ability.effect_entries;
                }

                var description = _descriptionGenerator.GenerateDescription(apiModel);
                var pokemon = new Domain.Model.Pokemon()
                {
                    Description = description,
                    Name = apiModel.name,
                };

                return pokemon;
            }

            throw new PokemonNotFoundException(request.Name);
        }
    }
}
