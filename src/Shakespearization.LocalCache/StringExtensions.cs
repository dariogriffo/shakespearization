namespace Shakespearization.LocalCache
{
    using System.Text.RegularExpressions;

    public static class StringExtensions
    {
        public static string ToSnakeCase(this string s)
        {
            return Regex.Replace(s, "[A-Z]", "_$0").ToLowerInvariant().TrimStart('_');
        }
    }
}
