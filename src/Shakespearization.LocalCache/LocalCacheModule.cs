﻿namespace Shakespearization.LocalCache
{
    using MediatR;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using MongoDB.Driver;
    using ServiceCollection.Extensions.Modules;
    using Shakespearization.Domain.Requests;
    using Shakespearization.LocalCache.Pipeline;

    public class LocalCacheModule : Module
    {
        protected override void Load(IServiceCollection services)
        {
            base.Load(services);
            services.AddSingleton<IMongoClient>(c =>
            {
                var connectionString = c.GetService<IConfiguration>().GetConnectionString("MongoDb");
                return new MongoClient(connectionString);
            });

            services.AddSingleton(c =>
            {
                var databaseName = c.GetService<IConfiguration>()["DatabaseName"] ?? "shakespearization";
                return c.GetService<IMongoClient>().GetDatabase(databaseName);
            });

            services.AddScoped<IPipelineBehavior<GetPokemon, Domain.Model.Pokemon>, GetCachedPokemonBehavior>();
        }
    }
}
