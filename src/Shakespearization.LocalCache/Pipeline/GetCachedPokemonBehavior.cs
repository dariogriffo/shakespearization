﻿namespace Shakespearization.LocalCache.Pipeline
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using MediatR;
    using Microsoft.Extensions.Logging;
    using MongoDB.Driver;
    using Shakespearization.Domain.Requests;

    public class GetCachedPokemonBehavior : IPipelineBehavior<GetPokemon, Domain.Model.Pokemon>
    {
        private static readonly InsertOneOptions DefaultInsertOptions = new InsertOneOptions() { BypassDocumentValidation = true };
        private readonly IMongoDatabase _database;
        private readonly ILogger<GetCachedPokemonBehavior> _logger;

        public GetCachedPokemonBehavior(IMongoDatabase database, ILogger<GetCachedPokemonBehavior> logger)
        {
            _database = database;
            _logger = logger;
        }

        public async Task<Domain.Model.Pokemon> Handle(GetPokemon request, CancellationToken cancellationToken, RequestHandlerDelegate<Domain.Model.Pokemon> next)
        {
            var collectionName = $"{typeof(Domain.Model.Pokemon).Name.ToSnakeCase()}s";
            var collection = _database.GetCollection<Pokemon>(collectionName);

            _logger.LogDebug("Trying to find Pokemon {0} in cache", request.Name);
            var cached = await
                collection
                    .Find(x => x.Name == request.Name)
                    .FirstOrDefaultAsync(cancellationToken);

            if (cached != null)
            {
                _logger.LogDebug("Pokemon {0} found in cache", request.Name);
                return new Domain.Model.Pokemon()
                {
                    Description = cached.Description,
                    Name = cached.Name,
                    TranslatedDescription = cached.TranslatedDescription,
                };
            }

            _logger.LogDebug("Pokemon {0} not found in cache", request.Name);

            var result = await next();

            if (result != null && string.IsNullOrEmpty(result.Description) == false)
            {
                _logger.LogDebug("Saving pokemon {0} in cache", request.Name);
                var document = new Pokemon()
                {
                    Description = result.Description,
                    Name = result.Name,
                    TranslatedDescription = result.TranslatedDescription,
                    Id = Guid.NewGuid(),
                };
                await collection.InsertOneAsync(document, DefaultInsertOptions, cancellationToken);
            }

            return result;
        }
    }
}
