﻿namespace Shakespearization.LocalCache.Pipeline
{
    using System;

    public class Pokemon
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string TranslatedDescription { get; set; }
    }
}
