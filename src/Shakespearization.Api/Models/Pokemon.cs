﻿namespace Shakespearization.Api.Models
{
    public class Pokemon
    {
        public Pokemon(Domain.Model.Pokemon pokemon)
        {
            Name = pokemon.Name;
            Description = pokemon.TranslatedDescription;
        }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
