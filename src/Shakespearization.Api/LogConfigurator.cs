namespace Shakespearization.Api
{
    using System;
    using System.Diagnostics;
    using System.Reflection;
    using System.Text;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using NLog.Extensions.Logging;
    using NLog.Targets;

    public static class LogConfigurator
    {
        public static IServiceCollection UseNLog(this IServiceCollection services)
        {
            Trace.CorrelationManager.ActivityId = Guid.NewGuid();
            var config = new NLog.Config.LoggingConfiguration();
            services.AddLogging(c =>
            {
                c.ClearProviders();
                c.SetMinimumLevel(LogLevel.Debug);
                c.AddNLog();
            });

            var attributes = new[]
            {
                "${date:universalTime=true:format=O}", "${level:uppercase=true:padding=-5}", "${activityid}",
                "${callsite:includeNamespace=false:cleanNamesOfAnonymousDelegates=true:cleanNamesOfAsyncContinuations=true}",
                "${message} ${exception:format=tostring}",
            };

            var text = new StringBuilder()
                .AppendJoin(" | ", attributes)
                .ToString();

            var simpleLayout = new NLog.Layouts.SimpleLayout()
            {
                Text = text,
            };

            var consoleTarget = new ColoredConsoleTarget("Console") { Layout = simpleLayout };

            var fileTarget = new FileTarget("File")
            {
                FileName = "logs/${var:applicationName}.log",
                ArchiveFileName = "logs/archive/${var:applicationName}_${shortdate}.log",
                ArchiveEvery = FileArchivePeriod.Day,
                MaxArchiveFiles = 1,
                KeepFileOpen = true,
                Encoding = Encoding.UTF8,
                Layout = simpleLayout,
            };

            config.AddRule(NLog.LogLevel.Debug, NLog.LogLevel.Fatal, consoleTarget);
            config.AddRule(NLog.LogLevel.Debug, NLog.LogLevel.Fatal, fileTarget);

            config.Variables["applicationName"] = Assembly.GetEntryAssembly().GetName().Name.ToLowerInvariant();

            // Apply config
            NLog.LogManager.Configuration = config;

            return services;
        }
    }
}
