﻿namespace Shakespearization.Api.Controllers
{
    using System.Threading;
    using System.Threading.Tasks;
    using MediatR;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Shakespearization.Domain.Requests;
    using Shakespearization.Pokemon;

    [ApiController]
    [Route("[controller]")]
    public class PokemonController : ControllerBase
    {
        private readonly ILogger<PokemonController> _logger;
        private readonly IMediator _mediator;

        public PokemonController(ILogger<PokemonController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet("{name}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get(string name, CancellationToken cancellationToken)
        {
            try
            {
                var pokemon = await _mediator.Send(new GetPokemon(name), cancellationToken);
                var model = new Models.Pokemon(pokemon);
                return Ok(model);
            }
            catch (PokemonNotFoundException)
            {
                _logger.LogWarning("Pokemon {0} not found", name);
                return NotFound(name);
            }
        }
    }
}
