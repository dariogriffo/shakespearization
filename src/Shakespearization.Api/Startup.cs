namespace Shakespearization.Api
{
    using System;
    using System.Diagnostics;
    using System.Reflection;
    using MediatR;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.OpenApi.Models;
    using ServiceCollection.Extensions.Modules;
    using Shakespearization.FunTranslations.Api;
    using Shakespearization.LocalCache;
    using Shakespearization.Pokemon;
    using Shakespearization.Pokemon.RequestHandlers;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.UseNLog();
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Shakespearization.Api", Version = "v1" });
            });
            services.AddHealthChecks();

            services.RegisterModule<LocalCacheModule>();
            services.RegisterModule<FunTranslationsApiModule>();
            services.RegisterModule<PokemonApiModule>();
            services.AddMediatR(typeof(GetPokemonFromPokeApiHandler).GetTypeInfo().Assembly);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.Use(async (context, next) =>
            {
                Trace.CorrelationManager.ActivityId = Guid.NewGuid();
                await next.Invoke();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Shakespearization.Api v1"));
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health");
                endpoints.MapControllers();
            });
        }
    }
}
