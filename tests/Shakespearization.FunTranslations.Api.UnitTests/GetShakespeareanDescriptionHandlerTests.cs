﻿namespace Shakespearization.FunTranslations.Api.UnitTests
{
    using System.Threading;
    using System.Threading.Tasks;
    using FluentAssertions;
    using Flurl.Http.Testing;
    using Microsoft.Extensions.Logging;
    using Moq;
    using NUnit.Framework;
    using Shakespearization.Domain.Requests;
    using Shakespearization.FunTranslations.Api.Configuration;
    using Shakespearization.FunTranslations.Api.Models.Response;
    using Shakespearization.FunTranslations.Api.Pipeline;
    using Shakespearization.FunTranslations.Api.Request;

    [TestFixture]
    public class GetShakespeareanDescriptionHandlerTests
    {
        [Test]
        public async Task Handle_WhenApiFailsToTranslate_ReturnsPokemonsDescription()
        {
            const string pokemon = "charizard";

            using var httpTest = new HttpTest();
            var response = new Root()
            {
                Success = new Success() { Total = 0 },
                Contents = new Contents()
                {
                    Translated = null,
                    Text = pokemon,
                    Translation = "shakespeare",
                },
            };

            httpTest.RespondWithJson(response);
            var configuration = new FunTranslationsApiConfiguration() { Url = "http://test.url" };

            var log = Mock.Of<ILogger<GetShakespeareanDescriptionBehavior>>();
            var queryParams = Mock.Of<IFunTranslationsRequestCreator>();

            Mock.Get(queryParams)
                .Setup(x => x.Create(It.IsAny<string>()))
                .Returns((string r) => new Body(r));

            var pokemon1 = new Domain.Model.Pokemon()
            {
                Description = "description",
            };

            Task<Domain.Model.Pokemon> Next()
            {
                return Task.FromResult(pokemon1);
            }

            var handler = new GetShakespeareanDescriptionBehavior(queryParams, log, configuration);
            var request = new GetPokemon(pokemon);
            var result = await handler.Handle(request, CancellationToken.None, Next);
            result.Description.Should().Be(pokemon1.Description);
        }

        [Test]
        public async Task Handle_WhenApiReturnsTranslation_ReturnsTranslatedMessage()
        {
            const string pokemon = "charizard";

            using var httpTest = new HttpTest();
            var response = new Root()
            {
                Success = new Success() { Total = 1 },
                Contents = new Contents()
                {
                    Translated =
                        "This pokémon cannot beest paralyzed. If 't be true a pokémon is paralyzed and acquires this ability,  its paralysis is did heal; this enwheels at which hour regaining a did lose ability upon leaving hurlyburly. This pokémon transforms into a by fate opponent upon entering hurlyburly. This effect is identical to the moveth transform.",
                    Text = pokemon,
                    Translation = "shakespeare",
                },
            };

            httpTest.RespondWithJson(response);

            var configuration = new FunTranslationsApiConfiguration() { Url = "http://test.url" };
            var log = Mock.Of<ILogger<GetShakespeareanDescriptionBehavior>>();
            var queryParams = Mock.Of<IFunTranslationsRequestCreator>();
            var mock = Mock.Get(queryParams);
            mock
                .Setup(x => x.Create(It.IsAny<string>()))
                .Returns((string r) => new Body(r));

            Task<Domain.Model.Pokemon> Next()
            {
                return Task.FromResult(new Domain.Model.Pokemon()
                {
                    TranslatedDescription = response.Contents.Translated,
                });
            }

            var handler = new GetShakespeareanDescriptionBehavior(queryParams, log, configuration);
            var request = new GetPokemon(pokemon);
            var result = await handler.Handle(request, CancellationToken.None, Next);
            result.TranslatedDescription.Should().Be(response.Contents.Translated);
        }
    }
}
