﻿namespace Shakespearization.FunTranslations.Api.UnitTests
{
    using System;
    using FluentAssertions;
    using NUnit.Framework;
    using Shakespearization.FunTranslations.Api.Request;

    [TestFixture]
    public class FunTranslationsQueryParamsTests
    {
        [Test]
        public void Create_WhenNameIsNull_ThrowsArgumentNullException()
        {
            var queryParams = new FunTranslationsRequestCreator();
            Action action = () => queryParams.Create(null);
            action.Should().Throw<ArgumentNullException>().And.ParamName.Should().Be("text");
        }

        [Test]
        public void Create_WhenTextIsNotNull_ReturnsNameAsParameter()
        {
            var text = Guid.NewGuid().ToString();

            var queryParams = new FunTranslationsRequestCreator();
            var result = queryParams.Create(text);
            var expected = new Body(text);
            result.Should().BeEquivalentTo(expected);
        }
    }
}
