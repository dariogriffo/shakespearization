﻿namespace Shakespearization.LocalCache.IntegrationTests
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using FluentAssertions;
    using Microsoft.Extensions.Logging;
    using Mongo2Go;
    using MongoDB.Driver;
    using Moq;
    using NUnit.Framework;
    using Shakespearization.Domain.Requests;
    using Shakespearization.LocalCache.Pipeline;

    [TestFixture]
    public class GetShakespeareanDescriptionHandlerCacheTests
    {
        [Test]
        public async Task WhenDataInDatabase_ReturnsLocalData()
        {
            using var runner = MongoDbRunner.Start();
            var client = new MongoClient(runner.ConnectionString);
            var cancellationToken = CancellationToken.None;
            var pokemon = new Pipeline.Pokemon()
            {
                Name = Guid.NewGuid().ToString(),
                Description = Guid.NewGuid().ToString(),
                Id = Guid.NewGuid(),
            };
            var expected = new Domain.Model.Pokemon()
            {
                Description = pokemon.Description,
                Name = pokemon.Name,
            };

            var database = client.GetDatabase("integration_tests");
            await database
                .GetCollection<Pipeline.Pokemon>($"{typeof(Domain.Model.Pokemon).Name.ToSnakeCase()}s")
                .InsertOneAsync(pokemon, cancellationToken: cancellationToken);

            static Task<Domain.Model.Pokemon> Next()
            {
                return Task.FromResult(new Domain.Model.Pokemon());
            }

            var log = Mock.Of<ILogger<GetCachedPokemonBehavior>>();
            var pipeline = new GetCachedPokemonBehavior(database, log);
            var request = new GetPokemon(pokemon.Name);
            var result = await pipeline.Handle(request, CancellationToken.None, Next);
            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }

        [Test]
        public async Task WhenNotInCache_ForwardsCallToApi()
        {
            using var runner = MongoDbRunner.Start();
            var client = new MongoClient(runner.ConnectionString);
            var database = client.GetDatabase("integration_tests");
            var expected = new Domain.Model.Pokemon()
            {
                Description = Guid.NewGuid().ToString(),
                Name = Guid.NewGuid().ToString(),
            };

            Task<Domain.Model.Pokemon> Next()
            {
                return Task.FromResult(expected);
            }

            var log = Mock.Of<ILogger<GetCachedPokemonBehavior>>();
            var pipeline = new GetCachedPokemonBehavior(database, log);
            var request = new GetPokemon(expected.Name);
            var result = await pipeline.Handle(request, CancellationToken.None, Next);
            result.Should().NotBeNull();
            result.Should().Be(expected);
        }

        [Test]
        public async Task WhenNotInCache_IfApiReturnsValidResult_ElementIsCached()
        {
            using var runner = MongoDbRunner.Start();
            var client = new MongoClient(runner.ConnectionString);
            var database = client.GetDatabase("integration_tests");
            var expected = new Domain.Model.Pokemon()
            {
                Description = Guid.NewGuid().ToString(),
                Name = Guid.NewGuid().ToString(),
            };

            Task<Domain.Model.Pokemon> Next()
            {
                return Task.FromResult(expected);
            }

            var log = Mock.Of<ILogger<GetCachedPokemonBehavior>>();
            var pipeline = new GetCachedPokemonBehavior(database, log);
            var request = new GetPokemon(expected.Name);
            _ = await pipeline.Handle(request, CancellationToken.None, Next);

            var result =
                await database
                    .GetCollection<Pipeline.Pokemon>($"{typeof(Domain.Model.Pokemon).Name.ToSnakeCase()}s")
                    .Find(x => x.Name == request.Name)
                    .FirstOrDefaultAsync(CancellationToken.None);

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }
    }
}
